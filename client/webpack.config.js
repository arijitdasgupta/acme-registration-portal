var path = require('path');

module.exports = {
    entry: './src/bundle.jsx',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js',
        publicPath: 'public'
    },

    devtool: 'source-map',

    devServer: {
        inline: true,
        port: 3000
    },

    module: {
        loaders: [
            {
                test : /\.jsx?/,
                loader : 'babel-loader'
            },
            { test: /\.scss$/, use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'sass-loader'
                }]
            }
        ]
    }
};
