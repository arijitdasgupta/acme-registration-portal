import React from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { updateForm } from '../actions/actions';

// Controlled component
export class TextInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
        }
    }

    render() {
        return <div className='inputDiv'>
            <input className={`${this.props.error?'error':''}`} type='text' placeholder={this.props.placeholder} onChange={this.props.onChange}
            { ... (this.state.value ? {value: this.state.value} : {}) } />
        </div>
    }
}

const TextInputWObjPath = (props) => {
    const createUpdateFormHandler = (objPath) => (ev) => {
        props.updateForm(objPath, ev.target.value);
    }

    return <TextInput onChange={createUpdateFormHandler(props.objPath)}
        value = { get(props.formState, props.objPath).value } 
        error = { get(props.formState, props.objPath).error }
        placeholder = {props.placeholder} />
}

export const TextInputConnected = connect((state, ownProps) => ({
    formState: state.subscriptionForm,
    objPath: ownProps.objPath,
    placeholder: ownProps.placeholder
}), (dispatch) => ({
    updateForm: (objPath, value) => dispatch(updateForm(objPath, value))
}))(TextInputWObjPath);