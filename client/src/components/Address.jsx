import React from 'react';
import { connect } from 'react-redux';

import { TextInputConnected } from './TextInput.jsx';
import { deleteAddress } from '../actions/actions';

export const AddressComponent = (props) => {
    const deleteAddressCreator = (index) => () => props.deleteAddress(index);

    return <div className='addressDiv boundedDiv'>
        <p className='formText'>Address <span className='clickable' onClick={deleteAddressCreator(props.index)}>[X]</span></p>

        <p className='smallText'>House Number</p>
        <TextInputConnected objPath={`address.${props.index}.number`} placeholder='House number' />
        <p className='smallText'>Street</p>
        <TextInputConnected objPath={`address.${props.index}.street`} placeholder='Street' />
        <p className='smallText'>City</p>
        <TextInputConnected objPath={`address.${props.index}.city`} placeholder='City' />
        <p className='smallText'>ZIP</p>
        <TextInputConnected objPath={`address.${props.index}.zipcode`} placeholder='ZIP' />
    </div>;
}

export const Address = connect((state, ownProps) => ({
    index: ownProps.index
}), (dispatch) => ({
    deleteAddress: (index) => dispatch(deleteAddress(index))
}))(AddressComponent);
