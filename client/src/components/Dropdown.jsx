import React from 'react';
import { isArray } from 'lodash';

// Controlled component
export class Dropdown extends React.Component {
    constructor(props) {
        super(props);

        // Sets defaul for textMapper
        if (!this.props.textMapper) {
            this.textMapper = i => i;
        } else {
            this.textMapper = this.props.textMapper;
        }

        // Sets detault for valueMapper
        if (!this.props.valueMapper) {
            this.valueMapper = i => i;
        } else {
            this.valueMapper = this.props.valueMapper
        }
        
        // Sets default for initial value
        this.state = {
            defaultValue: (props.defaultValue || this.valueMapper(props.items)) ? this.valueMapper(props.items[0]) : null
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            defaultValue: newProps.defaultValue ? newProps.defaultValue : null
        });
    }

    render() {
        const props = this.props;
        const items = isArray(props.items) ? props.items : [];
        return <div className='inputDiv'>
            <select defaultValue={this.state.defaultValue} onChange={props.onChange} >
                {items.map(item => <option key={this.valueMapper(item)} value={this.valueMapper(item)}>
                    {`${this.textMapper(item)}`}
                </option>)}
            </select>
        </div>;
    }
    
}