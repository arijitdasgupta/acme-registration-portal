import React from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { submitForm, addAddress } from '../actions/actions';
import { GenderDropdown } from './GenderDropdown.jsx';
import { TextInputConnected } from './TextInput.jsx';
import { Address } from './Address.jsx';

class FormClass extends React.Component {
    constructor(props) {
        super(props);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.addAddress = this.addAddress.bind(this);
    }

    handleFormSubmit(ev) {
        ev.preventDefault();
        this.props.submitForm();
    }

    addAddress(ev) {
        this.props.addAddress();
    }

    render() {
        return (<form onSubmit={this.handleFormSubmit}>
            <p>Please enter your details below</p>

            <div className='boundedDiv'>
                <TextInputConnected objPath = 'name' placeholder = 'Name' />
                <GenderDropdown objPath = 'gender' />
                <TextInputConnected objPath = 'email' placeholder = 'Email' />
                <TextInputConnected objPath = 'phone' placeholder = 'Phone' />
            </div>

            {this.props.formState.address.map((_, index) => <Address key={index} index={index} />)}

            <div className='inputDiv'>
                <button type='button' className='addAddress' onClick={this.addAddress}>Add address</button>
            </div>

            <div className='inputDiv'>
                <button className='formSubmit' type='submit'>Subscribe</button>
            </div>
        </form>);
    }
}

export const Form = connect((state) => ({
    formState: state.subscriptionForm
}), (dispatch) => ({
    submitForm: () => dispatch(submitForm),
    addAddress: () => dispatch(addAddress)
}))(FormClass);