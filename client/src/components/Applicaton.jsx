import React from 'react';
import { connect } from 'react-redux';

import { loadInitialData } from '../actions/actions';
import { Form } from './Form.jsx';
import { Error } from './Error.jsx';
import { Loading } from './Loading.jsx';
import { SubmissionConfirmation } from './SubmissionConfirmation.jsx';

export const ApplicationComponent = (props) => {
    return (<div className='wrapper'>
        <h1>Acme Subscription to catch the road runner!</h1>
        {props.error ? 
            <Error /> : ( props.loading ? 
                <Loading /> : ( props.submitted ? 
                    <SubmissionConfirmation /> : <Form /> ))}
    </div>);
}

export const Application = connect((state) => ({
    submitted: state.loadingFlags.submitted,
    loading: state.loadingFlags.loading,
    error: state.loadingFlags.error
}), (dispatch) => ({}))(ApplicationComponent);