import React from 'react';
import { get } from 'lodash';
import { connect } from 'react-redux';

import { Dropdown } from './Dropdown.jsx';
import { updateForm } from '../actions/actions';

const GenderDropdownComponent = (props) => {
    const createUpdateFormHandler = (objPath) => (ev) => {
        props.updateForm(objPath, ev.target.value)
    }

    return <Dropdown items = { get(props.formState, props.objPath).possibleValues } 
        defaultValue={get(props.formState, props.objPath).value}
        textMapper={x => ({male: 'Man', female: 'Woman', noident: 'Don\'t identify with gender'}[x])} 
        onChange={createUpdateFormHandler(props.objPath)} />;
}

export const GenderDropdown = connect((state, ownProps) => ({
    formState: state.subscriptionForm,
    objPath: ownProps.objPath,
}), (dispatch) => ({
    updateForm: (objPath, value) => dispatch(updateForm(objPath, value))
}))(GenderDropdownComponent);