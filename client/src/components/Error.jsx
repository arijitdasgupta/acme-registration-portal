import React from 'react';

export const Error = () => {
    return <div className='screen'>
        <p>Something went wrong!</p>
        <p className='screen-smallText'>Please refresh the page, check your inputs and try again</p>
    </div>;
}