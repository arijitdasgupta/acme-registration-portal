import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/main.scss';

import { MainWrapper } from './MainWrapper.jsx';

ReactDOM.render(<MainWrapper />, document.getElementById('root'));
