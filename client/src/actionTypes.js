export const actionTypes = {
    // Loading actions
    initialDataLoading: 'INITIAL_DATA_LOADING',
    initialDataLoaded: 'INITIAL_DATA_LOADED',
    dataLoadingError: 'DATA_LOADING_ERROR',

    // Form updated
    formUpdated: 'FORM_UPDATED',
    formValidationError: 'FORM_VALIDATION_ERROR',
    addAddress: 'ADD_ADDRESS',
    deleteAddress: 'DELETE_ADDRESS',

    // Submission actions
    subscriptionSubmitting: 'SUBSCRIPTION_SUBMITTING',
    subscriptionSubmitted: 'SUBSCRIPTION_SUBMITTED'
};
