import axios from 'axios';
import { get } from 'lodash';

import { actionTypes } from '../actionTypes';
import { config } from '../config';
import { isFormStateValid } from '../utils/validators';

const axiosClient = axios.create({
    baseURL: config.apiBaseUrl,
    timeout: 2000
});

export const updateForm = (objectPath, value) => (dispatch, getState) => {
    dispatch({
        type: actionTypes.formUpdated,
        payload: {
            objectPath,
            value
        }
    });
}

export const addAddress = (dispatch) => {
    dispatch({
        type: actionTypes.addAddress,
        payload: null
    });
}

export const deleteAddress = (index) => (dispatch) => {
    dispatch({
        type: actionTypes.deleteAddress,
        payload: index
    });
}

export const submitForm = async (dispatch, getState) => {
    console.log('being submitted');
    try {
        const formState = getState().subscriptionForm;

        if (isFormStateValid(formState)) {
            dispatch({
                type: actionTypes.subscriptionSubmitting,
                payload: null
            });

            // Creating HTTP payload to send
            const res = await axiosClient.post('/subscriptions', {
                name: formState.name.value,
                gender: formState.gender.value,
                email: formState.email.value,
                phone: formState.phone.value,
                address: formState.address.map(add => ({
                    street: add.street.value,
                    number: parseInt(add.number.value),
                    city: add.city.value,
                    zipcode: add.zipcode.value
                }))
            });
            dispatch({
                type: actionTypes.subscriptionSubmitted,
                payload: null
            });
        } else {
            dispatch({
                type: actionTypes.formValidationError,
                payaload: null
            });
        }
    } catch(e) {
        dispatch({
            type: actionTypes.dataLoadingError,
            payload: e
        });
    }
}