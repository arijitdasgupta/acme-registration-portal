import { isArray } from 'lodash';

export const isNumberStringValid = (value) => !isNaN(parseInt(value));
export const isStringValid = (value) => value && value.trim().length > 0;
// https://stackoverflow.com/questions/46155/how-can-you-validate-an-email-address-in-javascript
export const isEmailValid = (value) => value && value.match(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
export const isPhoneNumberValid = (value) => value && value.match(/^[0-9 \-]+$/);
export const isZipDutch = (value) => value && value.match(/^\d{4}\W?[a-z A-Z]{2}$/);

export const isFormStateValid = (state) => {
    return Object.keys(state).reduce((acc, key) => {
        return acc && (isArray(state[key]) ? 
            state[key].reduce((acc, ste) => isFormStateValid(ste), true) :
            state[key].validator(state[key].value));
    }, true);
}

// Mutates the state
export const sweepValidation = (state) => {
    Object.keys(state).forEach((key) => {
        if (isArray(state[key])) {
            state[key].forEach(ste => sweepValidation(ste));
        } else {
            state[key].error = !state[key].validator(state[key].value);
        }
    });
}