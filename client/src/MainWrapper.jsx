import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Provider } from 'react-redux';

import { Application } from './components/Applicaton.jsx';
import { reducer, initialState } from './reducers/reducer.js';

export const MainWrapper = () => {
    const store = createStore(reducer, applyMiddleware(logger, thunk));

    return (<Provider store={store}>
        <Application />
    </Provider>);
}