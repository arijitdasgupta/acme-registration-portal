import { actionTypes } from '../actionTypes';
import { clone } from 'lodash';

const initialState = {};

export const loadingFlags = (state, action) => {
    if (!state) {
        return initialState;
    }

    switch (action.type) {
        case actionTypes.initialDataLoading:
            state.loading = true;
            state.error = false;
            break;
        case actionTypes.initialDataLoaded:
            state.loading = false;
            state.error = false;
            break;
        case actionTypes.dataLoadingError:
            state.loading = false;
            state.error = true;
            break;
        case actionTypes.subscriptionSubmitting:
            state.loading = true;
            state.error = false;
            break;
        case actionTypes.subscriptionSubmitted:
            state.loading = false;
            state.error = false;
            state.submitted = true;
    }

    return clone(state);
}