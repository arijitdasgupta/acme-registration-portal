import { clone } from 'lodash';

import { actionTypes } from '../actionTypes';

const initialState = {};

export const subscription = (state, action) => {
    if (!state) {
        return initialState;
    }

    switch (action.type) {
        case actionTypes.subscriptionSubmitted:
            state = action.payload;
            break;
    }

    return clone(state);
}