import { combineReducers } from 'redux';

import { loadingFlags } from './loadingFlags';
import { subscriptionForm } from './subscriptionForm';
import { subscription } from './subscription';

export const reducer = combineReducers({
    loadingFlags,
    subscriptionForm,
    subscription
});