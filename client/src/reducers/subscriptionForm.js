import { clone, get, cloneDeep } from 'lodash';

import { actionTypes } from '../actionTypes';
import { isStringValid, isEmailValid, isZipDutch, isNumberStringValid, isPhoneNumberValid, sweepValidation } from '../utils/validators';

const blankAddress = {
    number: {
        value: null,
        error: false,
        validator: isNumberStringValid
    },
    street: {
        value: null,
        error: false,
        validator: isStringValid
    },
    city: {
        value: null,
        error: false,
        validator: isStringValid
    },
    zipcode: {
        value: null,
        error: false,
        validator: isZipDutch
    }
};

const initialState = {
    name: {
        value: null,
        error: false,
        validator: isStringValid
    },
    gender: {
        possibleValues: ['male', 'female', 'noident'],
        value: 'male',
        validator: isStringValid // This isn't actually needed, but just for uniformity's sake it's here
    },
    email: {
        value: null,
        error: false,
        validator: isEmailValid
    },
    phone: {
        value: null,
        error: false,
        validator: isPhoneNumberValid
    },
    address: [cloneDeep(blankAddress)]
};

export const subscriptionForm = (state, action) => {
    if (!state) {
        return initialState;
    }

    switch(action.type) {
        case (actionTypes.formUpdated):
            const objectPath = action.payload.objectPath;
            const value = action.payload.value;
            
            const valueObject = get(state, objectPath);

            valueObject.error = !valueObject.validator(value);
            valueObject.value = value;

            break;
        case (actionTypes.deleteAddress):
            state.address = state.address.filter((_, index) => index !== action.payload);
            break;
        case (actionTypes.addAddress):
            state.address.push(cloneDeep(blankAddress));
            break;
        case (actionTypes.formValidationError):
            // This one mutates the object, careful...
            sweepValidation(state);
            break;
        default:
            // nothing
    }

    // Can be optimized...
    return clone(state);
}