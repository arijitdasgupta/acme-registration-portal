import { isFormStateValid, sweepValidation } from '../../../src/utils/validators';

describe('validators', () => {
    describe('sweepValidation', () => {
        it('should set the error flag properly', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => false,
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => true
                        }
                    }
                ]
            };

            sweepValidation(state);
            expect(state.name.error).toEqual(true);
        });

        it('should set the error flag on nested arrays', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => false,
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => true,
                        }
                    }
                ]
            };

            sweepValidation(state);
            expect(state.name.error).toEqual(true);
        });
    });

    describe('isFormStateValid', () => {
        it('should validate form state and return apt result for no error', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => true
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => true
                        }
                    }
                ]
            }
            expect(isFormStateValid(state)).toEqual(true);
        });
    
        it('should validate form state and return apt result for some error', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => true
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => false
                        }
                    }
                ]
            }
            expect(isFormStateValid(state)).toEqual(false);
        });
    
        it('should validate form state and return apt result for some error in array', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => false
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => true
                        }
                    }
                ]
            }
            expect(isFormStateValid(state)).toEqual(false);
        });
    
        it('should validate form state and return apt result for some error in all', () => {
            const state = {
                name: {
                    value: '',
                    validator: x => true
                },
                bam: {
                    value: '',
                    validator: x => true
                },
                address: [
                    {number: 
                        {
                            value: '',
                            validator: x => false
                        }
                    }
                ]
            }
            expect(isFormStateValid(state)).toEqual(false);
        });
    });
});