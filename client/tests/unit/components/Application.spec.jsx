import * as testRenderer from 'react-test-renderer';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import React from 'react';
import { cloneDeep } from 'lodash';

import { Application } from '../../../src/components/Applicaton';

const primaryState = {
    subscriptionForm: {
        name: {
            value: null,
            error: false,
            validator: () => true
        },
        gender: {
            possibleValues: ['male', 'female', 'noident'],
            value: 'male',
            validator: () => true
        },
        email: {
            value: null,
            error: false,
            validator: () => true
        },
        phone: {
            value: null,
            error: false,
            validator: () => true
        },
        address: [{
            number: {
                value: null,
                error: false,
                validator: () => true
            },
            street: {
                value: null,
                error: false,
                validator: () => true
            },
            city: {
                value: null,
                error: false,
                validator: () => true
            },
            zipcode: {
                value: null,
                error: false,
                validator: () => true
            }
        }]
    },
    loadingFlags: {
        error: false,
        loading: false,
        submitted: false
    }
};

describe('Application', () => {
    it('should match the snapshot for state 1', () => {
        const store = createStore(state => state, cloneDeep(primaryState));
        const tree = testRenderer.create(<Provider store={store} >
            <Application />
        </Provider>).toJSON();

        expect(tree).toMatchSnapshot();
    });

    it('should match the snapshot for state 2', () => {
        const newState = cloneDeep(primaryState);
        newState.subscriptionForm.address.push(primaryState.subscriptionForm.address[0]);
        const store = createStore(state => state, cloneDeep(newState));
        const tree = testRenderer.create(<Provider store={store} >
            <Application />
        </Provider>).toJSON();

        expect(tree).toMatchSnapshot();
    });
});