import { updateForm, submitForm } from '../../../src/actions/actions';
import { actionTypes } from '../../../src/actionTypes';

describe('actions', () => {
    desribe('updateForm', () => {
        it('should call dispatch() properly', () => {
            const dispatch = jest.fn();
            updateForm('name', 'daz')(dispatch, () => {});
            expect(dispatch).toHaveBeenCalledWith({type: actionTypes.formUpdated, payload: {objectPath: 'name', value: 'daz'}});
        });
    });

    desribe('submitForm', () => {
        it('should call dispatch() properly', () => {
            const mockState = {
                subscriptionForm: {
                    name: {
                        value: '',
                        error: false,
                        validator: x => true,
                    },
                    address: [
                        {
                            number: {
                                value: '',
                                error: false,
                                validator: x => true
                            }
                        }
                    ]
                }
            }
            const dispatch = jest.fn();
            submitForm()(dispatch, () => mockState);
            expect(dispatch).toHaveBeenCalledWith({type: actionTypes.subscriptionSubmitting, payload: null});
        });

        // TODO add more test coverage.        
    });
});