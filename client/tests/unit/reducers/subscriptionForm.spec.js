import { subscriptionForm } from '../../../src/reducers/subscriptionForm';
import { actionTypes } from '../../../src/actionTypes';

describe('subscriptionForm', () => {
    describe('formUpdated', () => {
        it('should work for top level property', () => {
            const state = {
                name: {
                    value: null,
                    validator: x => false
                },
                address: [
                    {
                        number: {
                            value: '',
                            validator: x => true
                        }
                    }
                ]
            };
    
            const newState = subscriptionForm(state, {type: actionTypes.formUpdated, payload: {objectPath: 'name', value: 'meh'}});
            expect(newState.name.value).toEqual('meh');
        });

        it('should work for nested array property', () => {
            const state = {
                name: {
                    value: null,
                    validator: x => false
                },
                address: [
                    {
                        number: {
                            value: null,
                            validator: x => true
                        }
                    }
                ]
            };
    
            const newState = subscriptionForm(state, {type: actionTypes.formUpdated, payload: {objectPath: 'address.0.number', value: 'meh'}});
            expect(newState.address[0].number.value).toEqual('meh');
        });
    })
})