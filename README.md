# Acme Registration Portal

## Dev system

 - `docker` & `docker-compose`
 - `node >= 8.0.0` & `npm >= 5.0.0`

## To start
```bash
docker-compose up
# This starts a MongoDB container in your local docker machine.
```

```bash
cd api/

npm install
npm run watch

# In another terminal
npm run dev

# This start the API on localhost:8080
```

```bash
cd client/

npm install
npm run dev
# This starts the frontend dev server on localhost:3000
# Visit http://localhost:3000
```

### Building & starting for production
```bash
cd api/
npm run build
npm start
```

```bash
cd client/
npm run build;
```

For the client this produces a `build/` directory (that needs to be served from `/public`, and `index.html` needs to be served from the base path of a domain/IP since there are no `<base>` tag or configuration option in build time. When builing for production, make sure you set the `config.js` for the API URL.



## API endpoints

### `POST` `/subscriptions` with payload 

```json
{
    "name": "Buckley Patrick",
    "gender": "male",
    "email": "buckleypatrick@golistic.com",
    "phone": "0634 796837",
    "address": [{
        "number": 7,
        "street": "Adamsdreef",
        "city": "Rhenen",
        "zipcode": "6716ME"
    }]
}
```

Will register the user.

### `GET` `/subscriptions?offset=<number:0>limit=<number:0>` 

Returns,
```json
[{
    "name": "Buckley Patrick",
    "gender": "male",
    "email": "buckleypatrick@golistic.com",
    "phone": "0634 796837",
    "address": [{
        "number": 7,
        "street": "Adamsdreef",
        "city": "Rhenen",
        "zipcode": "6716ME"
    }]
},...
]
```

### `GET` `/subscriptions/schema`

Returns,
```json
{
    "type": "object",
    "required": [
        "name",
        "gender",
        "phone",
        "email",
        "address"
    ],
    "properties": {
        "name": {
            "type": "string"
        },
        "gender": {
            "type": "string",
            "pattern": "(male|female|noident)"
        },
        "email": {
            "type": "string",
            "format": "email"
        },
        "phone": {
            "type": "string"
        },
        "address": {
            "minItems": 1,
            "type": "array",
            "items": {
                "required": [
                    "number",
                    "street",
                    "city",
                    "zipcode"
                ],
                "type": "object",
                "properties": {
                    "number": {
                        "type": "number"
                    },
                    "street": {
                        "type": "string"
                    },
                    "city": {
                        "type": "string"
                    },
                    "zipcode": {
                        "type": "string",
                        "pattern": "^\\d{4}\\W?[a-z A-Z]{2}$"
                    }
                }
            }
        }
    }
}
```

An AJV (https://github.com/epoberezkin/ajv) compatible schema for validate the `POST` payload on the API level.


## Environment Variables for API node application
|ENV var|Default|Description|
|---------|--------|---------|
|PORT|8080|Port to run nodeJS server on|
|MONGO_HOST|mongodb://localhost:27018|Mongo connection string without the db|
|MONGO_DB_NAME|subscriptions|MongoDb name to store subscriptions data|
|CORS_DOMAIN|*|CORS header entry for cross site calls from browser|

TODOS:
 - Better presentaional aesthetic.
 - Improve UX.
 - Responsive design.
 - No checks to see if user has already registered.
 - Improves usage of the schema in the front-end.
 - Increase unit test coverage.
 - In client, no usage of history module etc, for reflective URL on the browser.
 - In client code, no use of React `propTypes`.
 - No CSRF protection.
 - Form validation routines invocations can be performance optimized by using reactive actions.
 - Not enough unit test coverage. But there are some.
 - No unit tests for reducers.
 - No production build.

 KNOWN BUG:
 - When multiple address are added, the red marker to indicate missing/bad form data is behaving non-optimally. [FIXED]

