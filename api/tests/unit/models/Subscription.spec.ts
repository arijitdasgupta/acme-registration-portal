import { describe, it, beforeEach, afterEach, before, after } from 'mocha';
import { expect } from 'chai';

import { SubscriptionsFactory } from '../../../src/models/Subscription';
import { BadRequest } from '../../../src/errors/BadRequest';

describe('Subscriptions', () => {
    it('should validate the objects', () => {
        const sub = (new SubscriptionsFactory()).getNew();

        const mockSubData = {
            name: 'meh',
            gender: 'male',
            email: 'a@a.com',
            phone: 'baba',
            address: [{
                number: 1,
                street: 'b',
                city: 'c',
                zipcode: '6716ME'
            }]
        };

        expect(sub.fromHTTPRequest(mockSubData)).to.deep.equal(mockSubData);
    });

    it('should throw error when bad data', () => {
        const sub = (new SubscriptionsFactory()).getNew();

        const mockSubData = {
            name: 'meh',
            gender: 'male',
            email: 'a@a.com',
            address: [{
                number: 1,
                street: 'b',
                city: 'c',
                zipcode: 'boom'
            }]
        };

        try {
            sub.fromHTTPRequest(mockSubData);
        } catch (e) {
            expect(e instanceof BadRequest).to.equal(true);
        }
    });

    it('should throw error when bad zipcode', () => {
        const sub = (new SubscriptionsFactory()).getNew();

        const mockSubData = {
            name: 'meh',
            gender: 'male',
            email: 'a@a.com',
            address: [{
                number: 1,
                street: 'b',
                city: 'c',
                zipcode: '245TG'
            }]
        };

        try {
            sub.fromHTTPRequest(mockSubData);
        } catch (e) {
            expect(e instanceof BadRequest).to.equal(true);
        }
    });
})