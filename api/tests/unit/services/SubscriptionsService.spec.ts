import { describe, it, beforeEach, afterEach, before, after } from 'mocha';
import { expect } from 'chai';
import * as chai from 'chai';
import * as spies from 'chai-spies';
import { Container } from 'inversify';
import { Db } from 'mongodb';
import 'reflect-metadata';

import { SubscriptionsService } from '../../../src/services/SubscriptionsService';
import { SubscriptionsFactory } from '../../../src/models/Subscription';
import { TYPES } from '../../../src/TYPES';

const mockSubData = {
    name: 'meh',
    gender: 'male',
    email: 'a@a.com',
    phone: 'baba',
    address: [{
        number: 1,
        street: 'b',
        city: 'c',
        zipcode: 'd'
    }]
};

chai.use(spies);

describe('SubscriptionService', () => {
    let fromDBObject, fromHTTPRequest, createSubscription, 
        getSubscriptions, mockSubscriptionFactory, mockSubscriptionsRepository,
        container, subscriptionsService;

    beforeEach(() => {
        fromDBObject = chai.spy(() => mockSubData);
        fromHTTPRequest = chai.spy(() => mockSubData);
        createSubscription = chai.spy(() => Promise.resolve(mockSubData));
        getSubscriptions = chai.spy(() => Promise.resolve([mockSubData, mockSubData]));

        mockSubscriptionsRepository = {
            createSubscription,
            getSubscriptions
        };

        mockSubscriptionFactory = {
            getNew: () => ({
                fromDBObject,
                fromHTTPRequest
            })
        };

        container = new Container();
        container.bind(TYPES.SubscriptionsRepository).toConstantValue(mockSubscriptionsRepository);
        container.bind(TYPES.SubscriptionsFactory).toConstantValue(mockSubscriptionFactory);
        container.bind(TYPES.SubscriptionsService).to(SubscriptionsService);

        subscriptionsService = container.get(TYPES.SubscriptionsService);
    });

    it('createSubscription should call apt functions', (cb) => {
        subscriptionsService.createSubscription(mockSubData).then(res => {
            expect(res).to.deep.equal(mockSubData);
            expect(fromHTTPRequest).to.have.been.called.with(mockSubData);
            expect(createSubscription).to.have.been.called.with(mockSubData);
            cb();
        }).catch(cb);
    });

    describe('getSubscriptions', () => {
        it('should pass the right parameters', (cb) => {
            subscriptionsService.getSubscriptions({limit: 6, offset: 0}).then(res => {
                expect(res).to.deep.equal([mockSubData, mockSubData]);
                expect(getSubscriptions).to.have.been.called.with(0, 6);
                cb();
            }).catch(cb);
        });

        it('should pass the default parameters', (cb) => {
            subscriptionsService.getSubscriptions({}).then(res => {
                expect(res).to.deep.equal([mockSubData, mockSubData]);
                expect(getSubscriptions).to.have.been.called.with(0, 10);
                cb();
            }).catch(cb);
        });
    });
});