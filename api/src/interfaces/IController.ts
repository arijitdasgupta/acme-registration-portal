import { Application } from 'express';

export interface IController {
    application: Application;
}