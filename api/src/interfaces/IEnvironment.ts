export interface IEnvironment {
    nodePort: number;
    corsDomain: string;
    mongoHost: string;
    mongoDbName: string;
}