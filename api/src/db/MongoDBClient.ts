import { inject, injectable } from 'inversify';
import { MongoClient, Db } from 'mongodb';

import { TYPES } from '../TYPES';
import { IEnvironment } from '../interfaces/IEnvironment';
import { IOHalter } from '../utils/IOHalter';

@injectable()
export class MongoDBClient {
    private env:IEnvironment;
    private ioHalter:IOHalter;
    public dbPromise:Promise<Db>;

    constructor(@inject(TYPES.Environment) env:IEnvironment, @inject(TYPES.IOHalter) ioHalter:IOHalter) {
        this.dbPromise = MongoClient.connect(env.mongoHost).then(client => client.db(env.mongoDbName));
        
        // Hatling HTTP I/O till connection to DB is established
        ioHalter.addPromise(this.dbPromise);
    }
}