import { injectable, inject } from 'inversify';
import * as express from 'express';
import * as bodyParser from 'body-parser';

import { TYPES } from '../TYPES';
import { IController } from '../interfaces/IController';
import { BadRequest } from '../errors/BadRequest';
import { ErrorHandlers } from '../utils/ErrorHandlers';
import { SubscriptionsService } from '../services/SubscriptionsService';
import { SubscriptionsFactory } from '../models/Subscription';

@injectable()
export class SubscriptionsController implements IController {
    application: express.Application;
    logger: any;
    errorHandlers: ErrorHandlers;
    subscriptionService: SubscriptionsService;
    subscriptionsFactory: SubscriptionsFactory;

    constructor(
        @inject(TYPES.Logger) logger,
        @inject(TYPES.ErrorHandlers) errorHandlers:ErrorHandlers,
        @inject(TYPES.SubscriptionsService) subscriptionService:SubscriptionsService,
        @inject(TYPES.SubscriptionsFactory) subscriptionsFactory:SubscriptionsFactory) {

        this.errorHandlers = errorHandlers;
        this.logger = logger;
        this.subscriptionService = subscriptionService;
        this.subscriptionsFactory = subscriptionsFactory;

        this.application = express();
        this.application.use(bodyParser.json());

        this.application.get('/subscriptions', this.getSubscriptions);
        this.application.post('/subscriptions', this.createSubscripton);
        this.application.get('/subscriptions/schema', this.getSubscriptionSchema);
    }

    getSubscriptionSchema:express.RequestHandler = async (req, res) => {
        res.send(this.subscriptionsFactory.getNew().getSchema());
    }

    getSubscriptions:express.RequestHandler = async (req, res) => {
        try {
            res.send(await this.subscriptionService.getSubscriptions(req.query));
        } catch (e) {
            this.errorHandlers.handleError(e, req, res);
        }
    }

    createSubscripton:express.RequestHandler = async (req, res) => {
        try {
            res.send(await this.subscriptionService.createSubscription(req.body));
        } catch (e) {
            this.errorHandlers.handleError(e, req, res);
        }
    }
}