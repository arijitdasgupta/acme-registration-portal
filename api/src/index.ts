import * as express from 'express';
import * as cors from 'cors';
import * as expressLogger from 'express-logging';

import { container } from './inversify.config';
import { TYPES } from './TYPES';
import { IEnvironment } from './interfaces/IEnvironment';
import { IController } from './interfaces/IController';
import { IOHalter } from './utils/IOHalter';

const env = container.get<IEnvironment>(TYPES.Environment);
const registrationController = container.get<IController>(TYPES.SubscriptionsController);
const ioHalter = container.get<IOHalter>(TYPES.IOHalter);
const logger = container.get<any>(TYPES.Logger);

const app = express();

app.set('port', env.nodePort);

app.use(expressLogger(container.get<any>(TYPES.Logger)));
app.use(cors({
    origin: env.corsDomain
}));
app.use('/api', registrationController.application);

app.get('/status', (req, res) => {
    res.send('OK');
});

// Waits for all the bootup processes to complete before starting up the HTTP I/O. That way 
// any docker orchestration infrastructure can switch containers approrpriately by doing health checks, to 
// the `/status` endpoint if neccessary.
Promise.all(ioHalter.getAllPromises()).then(() => {
    app.listen(env.nodePort, () => console.log(`REST API is running on port ${app.get('port')}!`));
}).catch((e) => {
    logger.error('Something went wrong while booting up', e);
});
