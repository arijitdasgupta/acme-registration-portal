import 'reflect-metadata';
import { Container } from 'inversify';

import { TYPES } from './TYPES';

import * as logger from 'logops';
import { IEnvironment } from './interfaces/IEnvironment';
import { IController } from './interfaces/IController';
import { Environemnt } from './env';
import { SubscriptionsController } from './controllers/SubscriptionsController';
import { SubscriptionsRepository } from './repositories/SubscriptionsRepository';
import { SubscriptionsService } from './services/SubscriptionsService';
import { ErrorHandlers } from './utils/ErrorHandlers';
import { MongoDBClient } from './db/MongoDBClient';
import { SubscriptionsFactory } from './models/Subscription';
import { IOHalter } from './utils/IOHalter';

const container = new Container();

// Utils, connections etc.
container.bind<IOHalter>(TYPES.IOHalter).to(IOHalter).inSingletonScope();
container.bind<any>(TYPES.Logger).toConstantValue(logger);
container.bind<IEnvironment>(TYPES.Environment).toConstantValue(Environemnt);
container.bind<MongoDBClient>(TYPES.MongoDBClient).to(MongoDBClient);
container.bind<ErrorHandlers>(TYPES.ErrorHandlers).to(ErrorHandlers);

// Controllers
container.bind<IController>(TYPES.SubscriptionsController).to(SubscriptionsController);

// Repositories
container.bind<SubscriptionsRepository>(TYPES.SubscriptionsRepository).to(SubscriptionsRepository);

// Services
container.bind<SubscriptionsService>(TYPES.SubscriptionsService).to(SubscriptionsService);

// Models
container.bind<SubscriptionsFactory>(TYPES.SubscriptionsFactory).to(SubscriptionsFactory);

export { container };