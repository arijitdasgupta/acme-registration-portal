export const TYPES = {
    Environment: Symbol(),
    SubscriptionsController: Symbol(),
    SubscriptionsRepository: Symbol(),
    SubscriptionsService: Symbol(),
    SubscriptionsFactory: Symbol(),
    Logger: Symbol(),
    IOHalter: Symbol(),
    ErrorHandlers: Symbol(),
    MongoDBClient: Symbol()
};