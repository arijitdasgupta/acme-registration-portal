import { inject, injectable } from 'inversify';

import { TYPES } from '../TYPES';
import { SubscriptionsFactory, ISubscriptionData } from '../models/Subscription';
import { SubscriptionsRepository } from '../repositories/SubscriptionsRepository';

@injectable()
export class SubscriptionsService {
    private subscriptionsRepository:SubscriptionsRepository;
    private subscriptionsFactory:SubscriptionsFactory;

    constructor(@inject(TYPES.SubscriptionsRepository) subscriptionsRepository:SubscriptionsRepository,
                @inject(TYPES.SubscriptionsFactory) subscriptionsFactory:SubscriptionsFactory) {

        this.subscriptionsRepository = subscriptionsRepository;
        this.subscriptionsFactory = subscriptionsFactory;
    }

    createSubscription = async (obj:any):Promise<ISubscriptionData> => {
        const validatedJSON = this.subscriptionsFactory.getNew().fromHTTPRequest(obj);
        return await this.subscriptionsRepository.createSubscription(validatedJSON);
    }

    getSubscriptions = (queryObject:any):Promise<ISubscriptionData[]> => {
        // setting defaults
        const limit = !isNaN(parseInt(queryObject.limit)) ? parseInt(queryObject.limit) : 10;
        const offset = !isNaN(parseInt(queryObject.offset)) ? parseInt(queryObject.offset) : 0;

        return this.subscriptionsRepository.getSubscriptions(offset, limit);
    }
}