import { injectable, inject } from 'inversify';
import { Db } from 'mongodb';

import { TYPES } from '../TYPES';
import { MongoDBClient } from '../db/MongoDBClient';
import { ISubscriptionData, SubscriptionsFactory } from '../models/Subscription';

@injectable()
export class SubscriptionsRepository {
    private db: Db;
    private subFactory: SubscriptionsFactory;
    private subscriptionsCollectionName = 'subscriptions';

    constructor(@inject(TYPES.MongoDBClient) mongoDbClient:MongoDBClient,
                @inject(TYPES.SubscriptionsFactory) subFactory:SubscriptionsFactory) {

        mongoDbClient.dbPromise.then(db => {
            this.db = db;
        });

        this.subFactory = subFactory;
    }

    createSubscription = (subscription:ISubscriptionData):Promise<ISubscriptionData> => {
        return this.db.collection(this.subscriptionsCollectionName).insert(subscription)
            .then(res => {
                const d = res.ops[0];
                return this.subFactory.getNew().fromDBObject(d)
            });
    }

    getSubscriptions = async (offset:number = 0, limit:number = 20):Promise<ISubscriptionData[]> => {
        const results = await this.db.collection(this.subscriptionsCollectionName).find({}).skip(offset).limit(limit).toArray();
        return results.map(doc => this.subFactory.getNew().fromDBObject(doc));
    }
}