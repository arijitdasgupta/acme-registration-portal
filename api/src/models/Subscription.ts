import { injectable, inject } from 'inversify';
import * as Ajv from 'ajv';

import { TYPES } from '../TYPES';
import { BadRequest } from '../errors/BadRequest';

export interface ISubscriptionData {
    name: string;
    gender: 'male' | 'female' | 'noident';
    email: string;
    phone: string;
    address: IAddress[];
}

export interface IAddress {
    number: number;
    street: string;
    city: string;
    zipcode: string;
}

class Subscription {
    constructor() {}

    // AJV compatible schema (https://github.com/epoberezkin/ajv)
    addressSchema = {
        required: ['number', 'street', 'city', 'zipcode'],
        type: 'object',
        properties: {
            number: { type: 'number' },
            street: { type: 'string' },
            city: { type: 'string' },
            zipcode: { type: 'string', pattern: '^\\d{4}\\W?[a-z A-Z]{2}$' } //TODO
        }
    }

    subscriptionSchema = {
        type: 'object',
        required: ['name', 'gender', 'phone', 'email', 'address'],
        properties: {
            name: { type: 'string' },
            gender: { type: 'string', pattern: '(male|female|noident)' },
            email: { type: 'string', format: 'email' },
            phone: { type: 'string' },
            address: {
                minItems: 1,
                type: 'array',
                items: this.addressSchema
            }
        }
    }

    fromHTTPRequest = (obj:any):ISubscriptionData => {
        const ajv = new Ajv();
        const validate = ajv.compile(this.subscriptionSchema);
        const valid = validate(obj);

        if (valid) {
            return {
                name: obj.name,
                gender: obj.gender,
                email: obj.email,
                phone: obj.phone,
                address: obj.address
            };
        } else {
            throw new BadRequest();
        }
    }

    fromDBObject = (obj:any):ISubscriptionData => {
        return {
            name: obj.name,
            gender: obj.gender,
            email: obj.email,
            phone: obj.phone,
            address: obj.address
        };
    }

    getSchema = () => this.subscriptionSchema;
}

@injectable()
export class SubscriptionsFactory {
    getNew() {
        return new Subscription();
    }
}