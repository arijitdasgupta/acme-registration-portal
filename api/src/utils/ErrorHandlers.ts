import { injectable, inject } from 'inversify';
import * as express from 'express';

import { TYPES } from '../TYPES';
import { BadRequest } from '../errors/BadRequest';

@injectable()
export class ErrorHandlers {
    logger: any;

    constructor(@inject(TYPES.Logger) logger) {
        this.logger = logger;
    }

    handleError = (err, req:express.Request, res:express.Response) => {
        this.logger.error(err);
        if (err instanceof BadRequest) {
            res.status(400).send(err);
        } else {
            res.status(500).send('Error occured');
        }
    }
}