import * as process from 'process';
import * as dotenv from 'dotenv';

dotenv.config();

import { IEnvironment } from './interfaces/IEnvironment';

export const Environemnt:IEnvironment = {
    nodePort: !isNaN(parseInt(process.env.PORT, 10)) ? parseInt(process.env.PORT) : 8080,
    mongoHost: process.env.MONGO_HOST || 'mongodb://localhost:27018',
    mongoDbName: process.env.MONGO_DB_NAME || 'subscriptions',
    corsDomain: process.env.CORS_DOMAIN || '*'
};